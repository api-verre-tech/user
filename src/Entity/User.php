<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get",
 *         "post",
 *         "login"={
 *              "controller"="App\Controller\UserController::login",
 *              "path"="/users/login",
 *              "method"="post",
 *              "openapi_context"={
 *                  "summary"="Connect to the application",
 *                  "requestBody"={
 *                      "content"={
 *                          "application/json"={
 *                               "schema"={
 *                                  "type"="object",
 *                                  "properties"={
 *                                      "email"={"type"="string", "example"="john.doe@viacesi.fr"},
 *                                      "password"={"type"="string", "example"="secretPassword"}
 *                                  }
 *                              },
 *                          }
 *                      }
 *                  },
 *                  "responses"={
 *                    "200"={
 *                        "description"="Token",
 *                        "content"={
 *                            "application/json"={
 *                                "schema"={
 *                                    "type"="object",
 *                                    "required"={"token"},
 *                                    "properties"={
 *                                        "token"={"type"="string"}
 *                                      }
 *                                  }
 *                              }
 *                          }
 *                      }
 *                  }
 *              },
 *          }
 *      },
 *     itemOperations={
 *         "get",
 *         "put",
 *         "patch"
 *     }
 * )
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="users",
 *      uniqueConstraints={@ORM\UniqueConstraint(name="users_email_unique",columns={"email"})}
 * )
 * @ApiFilter(SearchFilter::class, properties={"lastname": "partial", "firstname": "partial"})
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"intervention:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Email
     * @Assert\NotNull
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=2, max=255)
     * @Groups({"intervention:read"})
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=2, max=255)
     * @Groups({"intervention:read"})
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=2, max=255)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=5, max=255)
     * @Assert\NotNull
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=15)
     * @Assert\Length(min=0, max=15)
     */
    private $phone;

    /**
     * @ORM\OneToMany(targetEntity=Intervention::class, mappedBy="fkid_u")
     */
    private $interventions;

    /**
     * @ORM\OneToMany(targetEntity=InterventionTrack::class, mappedBy="fkid_u")
     */
    private $interventionTracks;

    /**
     * @ORM\Column(type="array")
     * @Assert\Type("array")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="integer")
     */
    private $withdrawalPoint;

    public function __construct()
    {
        $this->interventions = new ArrayCollection();
        $this->interventionTracks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return Collection|Intervention[]
     */
    public function getInterventions(): Collection
    {
        return $this->interventions;
    }

    public function addIntervention(Intervention $intervention): self
    {
        if (!$this->interventions->contains($intervention)) {
            $this->interventions[] = $intervention;
            $intervention->setFkidU($this);
        }

        return $this;
    }

    public function removeIntervention(Intervention $intervention): self
    {
        if ($this->interventions->removeElement($intervention)) {
            // set the owning side to null (unless already changed)
            if ($intervention->getFkidU() === $this) {
                $intervention->setFkidU(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|InterventionTrack[]
     */
    public function getInterventionTracks(): Collection
    {
        return $this->interventionTracks;
    }

    public function addInterventionTrack(InterventionTrack $interventionTrack): self
    {
        if (!$this->interventionTracks->contains($interventionTrack)) {
            $this->interventionTracks[] = $interventionTrack;
            $interventionTrack->setFkidU($this);
        }

        return $this;
    }

    public function removeInterventionTrack(InterventionTrack $interventionTrack): self
    {
        if ($this->interventionTracks->removeElement($interventionTrack)) {
            // set the owning side to null (unless already changed)
            if ($interventionTrack->getFkidU() === $this) {
                $interventionTrack->setFkidU(null);
            }
        }

        return $this;
    }

    public function getRoles()
    {
        $roles = $this->roles;

        // guarantee every user at least has ROLE_USER
        if (!in_array('ROLE_USER', $roles)) {
            $roles[] = 'ROLE_USER';
        }

        return $roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }


    public function getSalt()
    {
        return null;
    }

    public function getUsername()
    {
        return strtolower($this->firstname) . "." . strtolower($this->lastname);
    }

    public function eraseCredentials()
    {
        // Suppression des données sensibles
        //$this->plainPassword = null;
    }

    public function setWithdrawalPoint(int $withdrawalPoint): self
    {
        $this->withdrawalPoint = $withdrawalPoint;

        return $this;
    }


    public function getWithdrawalPoint(): ?int
    {
        return $this->withdrawalPoint;
    }
}
