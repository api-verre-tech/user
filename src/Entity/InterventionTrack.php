<?php

namespace App\Entity;

use App\Repository\InterventionTrackRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=InterventionTrackRepository::class)
 * @ApiResource()
 * @ApiFilter(SearchFilter::class)
 */
class InterventionTrack
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="interventionTracks")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull
     * @Groups({"intervention:read"})
     */
    private $fkid_u;

    /**
     * @ORM\ManyToOne(targetEntity=Intervention::class, inversedBy="interventionTracks")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull
     */
    private $fkid_inter;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=0, max=255)
     * @Groups({"intervention:read"})
     */
    private $message;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"intervention:read"})
     */
    private $time;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFkidU(): ?User
    {
        return $this->fkid_u;
    }

    public function setFkidU(?User $fkid_u): self
    {
        $this->fkid_u = $fkid_u;

        return $this;
    }

    public function getFkidInter(): ?Intervention
    {
        return $this->fkid_inter;
    }

    public function setFkidInter(?Intervention $fkid_inter): self
    {
        $this->fkid_inter = $fkid_inter;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getTime(): ?\DateTimeInterface
    {
        return $this->time;
    }

    public function setTime(\DateTimeInterface $time): self
    {
        $this->time = $time;

        return $this;
    }
}
