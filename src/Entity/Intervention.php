<?php

namespace App\Entity;

use App\Repository\InterventionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=InterventionRepository::class)
 * @ApiResource(
 *     normalizationContext={"groups"={"intervention:read"}},
 * )
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "name": "partial", "fkid_u": "exact", "fkid_interType": "exact"})
 */
class Intervention
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"intervention:read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="interventions")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull
     * @Groups({"intervention:read"})
     */
    private $fkid_u;

    /**
     * @ORM\ManyToOne(targetEntity=InterventionType::class, inversedBy="interventions")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull
     * @Groups({"intervention:read"})
     */
    private $fkid_interType;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=0, max=255)
     * @Groups({"intervention:read"})
     */
    private $message;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"intervention:read"})
     */
    private $time;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\Type("boolean")
     * @Groups({"intervention:read"})
     */
    private $handled;

    /**
     * @ORM\OneToMany(targetEntity=InterventionTrack::class, mappedBy="fkid_inter")
     * @Groups({"intervention:read"})
     */
    private $interventionTracks;

    public function __construct()
    {
        $this->interventionTracks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFkidU(): ?User
    {
        return $this->fkid_u;
    }

    public function setFkidU(?User $fkid_u): self
    {
        $this->fkid_u = $fkid_u;

        return $this;
    }

    public function getFkidInterType(): ?InterventionType
    {
        return $this->fkid_interType;
    }

    public function setFkidInterType(?InterventionType $fkid_interType): self
    {
        $this->fkid_interType = $fkid_interType;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getTime(): ?\DateTimeInterface
    {
        return $this->time;
    }

    public function setTime(\DateTimeInterface $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getHandled(): ?bool
    {
        return $this->handled;
    }

    public function setHandled(bool $handled): self
    {
        $this->handled = $handled;

        return $this;
    }

    /**
     * @return Collection|InterventionTrack[]
     */
    public function getInterventionTracks(): Collection
    {
        return $this->interventionTracks;
    }

    public function addInterventionTrack(InterventionTrack $interventionTrack): self
    {
        if (!$this->interventionTracks->contains($interventionTrack)) {
            $this->interventionTracks[] = $interventionTrack;
            $interventionTrack->setFkidInter($this);
        }

        return $this;
    }

    public function removeInterventionTrack(InterventionTrack $interventionTrack): self
    {
        if ($this->interventionTracks->removeElement($interventionTrack)) {
            // set the owning side to null (unless already changed)
            if ($interventionTrack->getFkidInter() === $this) {
                $interventionTrack->setFkidInter(null);
            }
        }

        return $this;
    }
}
