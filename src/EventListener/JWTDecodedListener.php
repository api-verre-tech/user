<?php

// src/EventListener/JWTDecodedListener.php

namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTDecodedEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class JWTDecodedListener
{

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var CsrfTokenManagerInterface
     */
    private $csrfTokenManager;

    /**
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack, CsrfTokenManagerInterface $csrfTokenManager)
    {
        $this->requestStack = $requestStack;
        $this->csrfTokenManager = $csrfTokenManager;
    }

    /**
     * @param JWTDecodedEvent $event
     *
     * @return void
     */
    public function onJWTDecoded(JWTDecodedEvent $event)
    {
        $request = $this->requestStack->getCurrentRequest();
        
        $payload = $event->getPayload();
        
        if (!isset($payload['csrfToken']) || $payload['csrfToken'] !== $request->headers->get('x-xsrf-token')) {
            $event->markAsInvalid();
        }
    }
}
