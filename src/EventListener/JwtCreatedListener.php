<?php

// src/EventListener/JwtCreatedListener.php

namespace App\EventListener;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class JwtCreatedListener
{

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var CsrfTokenManagerInterface
     */
    private $csrfTokenManager;

    /**
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack, EntityManagerInterface $entityManager, CsrfTokenManagerInterface $csrfTokenManager)
    {
        $this->requestStack = $requestStack;
        $this->entityManager = $entityManager;
        $this->csrfTokenManager = $csrfTokenManager;
    }

    /**
     * @param JWTCreatedEvent $event
     *
     * @return void
     */
    public function onJWTCreated(JWTCreatedEvent $event)
    {
        $request = $this->requestStack->getCurrentRequest();

        $payload = $event->getData();

        $user = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $payload['email']]);

        $expiration = new \DateTime('+1 day');
        $expiration->setTime(2, 0, 0);
        $payload['exp'] = $expiration->getTimestamp();

        $payload['name'] = $user->getFirstname();
        $payload['id'] = $user->getId();
        $payload['withdrawalPoint'] = $user->getWithdrawalPoint();

        $payload['csrfToken'] = $this->csrfTokenManager->refreshToken('authenticate')->getValue();
        $event->setData($payload);
    }
}
