<?php
// src/DataPersister/UserDataPersister.php

namespace App\DataPersister;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Utils\CallAPI;
use Prophecy\Prediction\CallPrediction;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 *
 */
class UserDataPersister implements ContextAwareDataPersisterInterface
{
    private $_entityManager;
    private $_passwordEncoder;
    private $_callApi;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $passwordEncoder,
        CallAPI $callApi
    ) {
        $this->_entityManager = $entityManager;
        $this->_passwordEncoder = $passwordEncoder;
        $this->_callApi = $callApi;
    }

    /**
     * {@inheritdoc}
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof User;
    }

    /**
     * @param User $data
     */
    public function persist($data, array $context = [])
    {
        if ($data->getPassword()) {
            if(
                ($context['collection_operation_name'] ?? null) === 'post' ||
                ($context['graphql_operation_name'] ?? null) === 'create'
                ) {
                    $data->setPassword(
                        $this->_passwordEncoder->encodePassword(
                            $data,
                            $data->getPassword()
                        )
                    );

                    $response = HttpClient::create()->request(
                        "GET",
                        "https://api-adresse.data.gouv.fr/search/?q=". $data->getAddress() ."&limit=1"
                    );
                    $addressInfo = $response->toArray()['features'];

                    $nameCity = $addressInfo[0]['properties']['city'];

                    $r = $this->_callApi->fetch("GET", "store", "/cities?name=" . $nameCity);
                    if($r->getStatusCode() !== 200) {
                        $r->getContent();
                    }
                    $city = $r->toArray()["hydra:member"];
                    if(sizeof($city) == 0) {
                        // si la ville n'existe pas, on l'ajoute
                        $response = HttpClient::create()->request(
                            "GET",
                            "https://api-adresse.data.gouv.fr/search/?q=". $nameCity ."&limit=1"
                        );
                        $cityInfo = $response->toArray()['features'];
                        
                        $dataCity['name'] = $nameCity;
                        $dataCity['postalCode'] = $cityInfo[0]['properties']['postcode'];
                        $dataCity['longitude'] = strval($cityInfo[0]['geometry']['coordinates'][0]);
                        $dataCity['latitude'] = strval($cityInfo[0]['geometry']['coordinates'][1]);

                        $r = $this->_callApi->fetch("POST","store","/cities", $dataCity);
                        if($r->getStatusCode() !== 200) {
                            $r->getContent();
                        }
                        $city = $r->toArray();
                    }else {
                        $city = $city[0];
                    }
                    $data->setWithdrawalPoint($city["id"]);
                }
            $data->eraseCredentials();
        }

        $this->_entityManager->persist($data);
        $this->_entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function remove($data, array $context = [])
    {
        $this->_entityManager->remove($data);
        $this->_entityManager->flush();
    }
}
