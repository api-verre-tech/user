<?php

namespace App\Repository;

use App\Entity\InterventionTrack;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method InterventionTrack|null find($id, $lockMode = null, $lockVersion = null)
 * @method InterventionTrack|null findOneBy(array $criteria, array $orderBy = null)
 * @method InterventionTrack[]    findAll()
 * @method InterventionTrack[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InterventionTrackRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InterventionTrack::class);
    }

    // /**
    //  * @return InterventionTrack[] Returns an array of InterventionTrack objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InterventionTrack
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
