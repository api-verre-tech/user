<?php

namespace App\Utils;

use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Contracts\HttpClient\ResponseInterface;

class CallAPI
{
    public AdapterInterface $cache;

    public function __construct(AdapterInterface $cache) {
        $this->cache = $cache;
    }
    
    public function fetch(String $method, String $service, String $url, array $param = []): ResponseInterface
    {
        $client = HttpClient::create();
        $dotenv = new Dotenv();
        $dotenv->load(__DIR__ . '/../../.env');
        $apiKeyCache = $this->cache->getItem('user_apiKey');
        $credentials = [
            "email" => $_ENV['API_KEY_LOGIN'],
            "password" => $_ENV['API_KEY_SECRET']
        ];

        if (!$apiKeyCache->isHit()) {
            $response = $client->request(
                "POST",
                'http://host.docker.internal:' . $_ENV["USER_PORT"] . '/api/users/login',
                    [
                        'json' => $credentials
                    ]
            );
            if($response->getStatusCode() !== 200) {
                $response->getContent();
            }
            $r_login = $response->toArray();

            $apiKeyCache->set($r_login['token']);
            $this->cache->save($apiKeyCache);
        }

        $jwtPayload = json_decode(base64_decode(explode(".", $apiKeyCache->get())[1]));

        $servicePort = $_ENV[strtoupper($service) . "_PORT"];
        $response = $client->request(
            $method,
            'http://host.docker.internal:' . $servicePort . '/api' . $url,
            $method!="PATCH"
                ?[
                    'headers' => [
                        'Authorization' => 'Bearer ' . $apiKeyCache->get(), 
                        'x-xsrf-token' => $jwtPayload->csrfToken
                    ],
                    'json' => $param
                ]
                :[
                    'headers' => [
                        'Content-Type' => 'application/merge-patch+json',
                        'Authorization' => 'Bearer ' . $apiKeyCache->get(),
                        'x-xsrf-token' => $jwtPayload->csrfToken
                    ],
                    'json' => $param
                ]
        );
        // On vérifie si le token à expirer et que le MS à renvoyer 401
        if($response->getStatusCode() == 401) {
            $response = $client->request(
                "POST",
                'http://host.docker.internal:' . $_ENV["USER_PORT"] . '/api/users/login',
                    [
                        'json' => $credentials
                    ]
            );
            if($response->getStatusCode() !== 200) {
                $response->getContent();
            }
            $r_login = $response->toArray();

            $apiKeyCache->set($r_login['token']);
            $this->cache->save($apiKeyCache);

            // Après la réponse, on relance l'appel
            $response = $this->fetch($method, $service, $url, $param);
        }
        return $response;
    }
}
