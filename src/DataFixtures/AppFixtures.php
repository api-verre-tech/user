<?php

namespace App\DataFixtures;

use App\Entity\InterventionType;
use App\Entity\Intervention;
use App\Entity\InterventionTrack;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use App\Entity\User;
use DateTime;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $e)
    {
        $this->encoder = $e;
    }

    public function load(ObjectManager $manager)
    {
        $useralex = new User();
        $useralex->setAddress("2 Rue Molière 51100 Reims");
        $useralex->setFirstname("Alexandre");
        $useralex->setLastname("VARIN");
        $useralex->setPhone("0600000000");
        $useralex->setEmail("alexandre.varin1@viacesi.fr");
        $useralex->setWithdrawalPoint(1);
        $pass = $this->encoder->encodePassword($useralex, "didix");
        $useralex->setPassword($pass);
        $useralex->setRoles(['ROLE_ADMIN']);
        $manager->persist($useralex);
        
        $user = new User();
        $user->setAddress("114 Rue Jeanne d'Arc, 54000 Nancy");
        $user->setFirstname("Morgane");
        $user->setLastname("DIDELOT");
        $user->setPhone("0600000000");
        $user->setEmail("morgane.didelot@viacesi.fr");
        $user->setWithdrawalPoint(2);
        $pass = $this->encoder->encodePassword($user, "momo");
        $user->setPassword($pass);
        $user->setRoles(['ROLE_ADMIN']);
        $manager->persist($user);
        
        $user = new User();
        $user->setAddress("14 Rue du Général Hulot, 54000 Nancy");
        $user->setFirstname("Ahmet");
        $user->setLastname("ACIKGOZ");
        $user->setPhone("0600000000");
        $user->setWithdrawalPoint(2);
        $user->setEmail("ahmet.acikgoz@viacesi.fr");
        $pass = $this->encoder->encodePassword($user, "bat");
        $user->setPassword($pass);
        $user->setRoles(['ROLE_ADMIN']);
        $manager->persist($user);
        
        $userjohn = new User();
        $userjohn->setAddress("2 Rue du Faubourg des Postes, 59000 Lille");
        $userjohn->setFirstname("John");
        $userjohn->setLastname("DOE");
        $userjohn->setPhone("0600000000");
        $userjohn->setEmail("john.doe@viacesi.fr");
        $userjohn->setWithdrawalPoint(3);
        $pass = $this->encoder->encodePassword($userjohn, "doe");
        $userjohn->setPassword($pass);
        $manager->persist($userjohn);

        $user = new User();
        $user->setAddress("20 Rue Louis Hestaux 57070 Metz");
        $user->setFirstname("Ano");
        $user->setLastname("NYMOUS");
        $user->setPhone("0600000000");
        $user->setEmail("ano.nymous@viacesi.fr");
        $user->setWithdrawalPoint(8);
        $pass = $this->encoder->encodePassword($user, "anono");
        $user->setPassword($pass);
        $manager->persist($user);

        $user = new User();
        $user->setAddress("Microservice ORDER");
        $user->setFirstname("order");
        $user->setLastname("ms");
        $user->setPhone("");
        $user->setEmail("order@microservice.fr");
        $pass = $this->encoder->encodePassword($user, "*_MS_ORDER_API_KEY*_");
        $user->setPassword($pass);
        $user->setWithdrawalPoint(0);
        $manager->persist($user);
        
        $user = new User();
        $user->setAddress("Microservice PAYMENT");
        $user->setFirstname("payment");
        $user->setLastname("ms");
        $user->setPhone("");
        $user->setEmail("payment@microservice.fr");
        $pass = $this->encoder->encodePassword($user, "*_MS_PAYMENT_API_KEY*_");
        $user->setPassword($pass);
        $user->setWithdrawalPoint(0);
        $manager->persist($user);
        
        $user = new User();
        $user->setAddress("Microservice STORE");
        $user->setFirstname("store");
        $user->setLastname("ms");
        $user->setPhone("");
        $user->setEmail("store@microservice.fr");
        $pass = $this->encoder->encodePassword($user, "*_MS_STORE_API_KEY*_");
        $user->setPassword($pass);
        $user->setWithdrawalPoint(0);
        $manager->persist($user);
        
        $user = new User();
        $user->setAddress("Microservice USER");
        $user->setFirstname("user");
        $user->setLastname("ms");
        $user->setPhone("");
        $user->setEmail("user@microservice.fr");
        $pass = $this->encoder->encodePassword($user, "*_MS_USER_API_KEY*_");
        $user->setPassword($pass);
        $user->setWithdrawalPoint(0);
        $manager->persist($user);
        
        $user = new User();
        $user->setAddress("Microservice FRONT");
        $user->setFirstname("front");
        $user->setLastname("ms");
        $user->setPhone("");
        $user->setEmail("front@microservice.fr");
        $pass = $this->encoder->encodePassword($user, "*_MS_FRONT_API_KEY*_");
        $user->setPassword($pass);
        $user->setWithdrawalPoint(0);
        $manager->persist($user);
        
        $user = new User();
        $user->setAddress("Microservice PRODUCT");
        $user->setFirstname("product");
        $user->setLastname("ms");
        $user->setPhone("");
        $user->setEmail("product@microservice.fr");
        $pass = $this->encoder->encodePassword($user, "*_MS_PRODUCT_API_KEY*_");
        $user->setPassword($pass);
        $user->setWithdrawalPoint(0);
        $manager->persist($user);

        $intervention_type = new InterventionType();
        $intervention_type->setName("Anomalie");
        $manager->persist($intervention_type);

        $intervention_type = new InterventionType();
        $intervention_type->setName("Suivi commande");
        $manager->persist($intervention_type);
        
        $intervention_type = new InterventionType();
        $intervention_type->setName("Modification");
        $manager->persist($intervention_type);

        $intervention = new Intervention();
        $intervention->setFkidU($userjohn);
        $intervention->setFkidInterType($intervention_type);
        $intervention->setMessage("Bonjour <br /> j'aimerai bien modifier le nombre de porte que j'ai acheté pour le passer à trois.<br />Merci");
        $intervention->setTime(new DateTime('now'));
        $intervention->setHandled(false);
        $manager->persist($intervention);

        $inter_track = new InterventionTrack();
        $inter_track->setFkidU($useralex);
        $inter_track->setMessage("Bonjour <br /> Vous êtes sûrs ? Il faudra payer plus<br />Merci");
        $inter_track->setTime(new DateTime('now'));
        $inter_track->setFkidInter($intervention);
        $manager->persist($inter_track);


        $manager->flush();
    }
}
