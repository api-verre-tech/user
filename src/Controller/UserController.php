<?php
// api/src/Controller/UserController.php

namespace App\Controller;

use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class UserController extends AbstractController
{
    public function login(AuthenticationUtils $authenticationUtils, JWTTokenManagerInterface $JWTManager): JsonResponse
    {
        $response = new JsonResponse(
            array(
                "code" => Response::HTTP_NOT_FOUND,
                "message" =>"Informations invalide"
            ),
            Response::HTTP_NOT_FOUND,
            ['content-type' => 'application/json']
        );
        if ($this->getUser()) {
            $response = new JsonResponse(
                array(
                    'token' => $JWTManager->create($this->getUser())
                ),
                Response::HTTP_OK,
                ['content-type' => 'application/json']
            );
        }
        
        return $response;
    }

    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
