<?php

namespace App\Tests\Controller;

use ApiPlatform\Core\Serializer\JsonEncoder;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

class UserControllerTest extends WebTestCase
{
    private $encoder;

    protected function setUp() : void
    {
        self::bootKernel();
        $this->encoder = self::$container->get('Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface');
    }

    /**
     * Test that the URL for the documentation are OK and protected
     */
    public function testApiPlateform()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();

        // This link HAS TO BE protected in production
        // Every MS that call user has to be authenticated
        $client->request('GET', '/api');

        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        //$this->assertEquals(200, $client->getResponse()->getStatusCode());

        $client->request('GET', '/api/docs');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    
        $this->assertResponseIsSuccessful();
        $this->assertStringContainsString(
            'User',
            $client->getResponse()->getContent(),
            "La documentation API Platform ne liste pas l'entité User"
        );
    }

    /**
     * Test login + JWT
     * Assert that call api/users/login and it returned is OK
     * Assert that JWT is generate and is data is OK
     */
    public function testLogin()
    {
        $dotenv = new Dotenv();
        $dotenv->load(__DIR__ . '/../../.env');

        self::ensureKernelShutdown();
        $client = static::createClient();

        $email = "alexandre.varin1@viacesi.fr";
        $client->request(
            'POST',
            '/api/users/login',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode(array(
              'email' => $email,
              'password' => "didix",
              ))
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        
        $data = json_decode($client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('token', $data);

        $this->jwtToken = $data['token'];
        $jwtPayload = json_decode(base64_decode(explode(".", $data['token'])[1]));
        
        // Check Payload
        // Expect data :
        /**..object(stdClass)#2189 (6) {
             ["iat"]=>
            int(...)
            ["exp"]=>
            int(...)
            ["roles"]=>
            array(2) {
                [0]=>
                string(10) "ROLE_ADMIN"
                [1]=>
                string(9) "ROLE_USER"
            }
            ["email"]=>
            string(27) "alexandre.varin1@viacesi.fr"
            ["name"]=>
            string(9) "Alexandre"
            ["ip"]=>
            string(9) "127.0.0.1"
            } */
        $this->assertEquals($email, $jwtPayload->email);
        $this->assertEquals("Alexandre", $jwtPayload->name);
        $this->assertContainsEquals("ROLE_ADMIN", $jwtPayload->roles);
        $this->assertContainsEquals("ROLE_USER", $jwtPayload->roles);
    }

    /**
     * Test a creation of user with the API and assert data are correct
     */
    public function testCreateUserApi()
    {
        $user = new User();
        $user->setAddress("24 allé du bois");
        $user->setFirstname("Ano");
        $user->setLastname("NYMOUS");
        $user->setPhone("0600000000");
        $user->setEmail("test.id". rand() . "@viacesi.fr");
        $pass = "anony";
        $user->setPassword($pass);
        $user->setWithdrawalPoint(1);
        $user->setRoles(['ROLE_ADMIN']);

        $this->testLogin(); // pour avoir le jwt

        self::ensureKernelShutdown();
        $client = static::createClient();
        $serializer = new Serializer(array(new GetSetMethodNormalizer()), array(new JsonEncoder('json')));
        
        $userJson = $serializer->serialize($user, 'json');

        $this->assertGreaterThan(0, strlen($userJson)); // check if serialization is ok

        $jwtPayload = json_decode(base64_decode(explode(".", $this->jwtToken)[1]));

        $client->request(
            'POST',
            '/api/users',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json', 'HTTP_Authorization' => 'Bearer ' . $this->jwtToken, 'HTTP_x-xsrf-token' => $jwtPayload->csrfToken),
            $userJson
        );

        $this->assertEquals(201, $client->getResponse()->getStatusCode());

        $response = $client->getResponse()->getContent();

        $userResponse = $serializer->deserialize($response, User::class, 'json');

        $this->assertEquals($userResponse->getFirstname(), $user->getFirstname());
        $this->assertEquals($userResponse->getEmail(), $user->getEmail());
        $this->assertEquals($userResponse->getLastname(), $user->getLastname());
        $this->assertTrue($this->encoder->isPasswordValid($userResponse, $user->getPassword()));
        $this->assertEquals($userResponse->getPhone(), $user->getPhone());
        $this->assertEquals($userResponse->getRoles(), $user->getRoles());
        $this->assertEquals($userResponse->getUsername(), $user->getUsername());
        $this->assertEquals($userResponse->getInterventions(), $user->getInterventions());
        $this->assertEquals($userResponse->getAddress(), $user->getAddress());
        $this->assertEquals($userResponse->getInterventionTracks(), $user->getInterventionTracks());
        
        // Check if the created user is in the database
        $client->request('GET', '/api/users/' . $userResponse->getId());
        $response = $client->getResponse()->getContent();
        $userResponse = $serializer->deserialize($response, User::class, 'json');

        $this->assertEquals($userResponse->getFirstname(), $user->getFirstname());
        $this->assertEquals($userResponse->getEmail(), $user->getEmail());
        $this->assertEquals($userResponse->getLastname(), $user->getLastname());
        $this->assertTrue($this->encoder->isPasswordValid($userResponse, $user->getPassword()));
        $this->assertEquals($userResponse->getPhone(), $user->getPhone());
        $this->assertEquals($userResponse->getRoles(), $user->getRoles());
        $this->assertEquals($userResponse->getUsername(), $user->getUsername());
        $this->assertEquals($userResponse->getInterventions(), $user->getInterventions());
        $this->assertEquals($userResponse->getAddress(), $user->getAddress());
        $this->assertEquals($userResponse->getInterventionTracks(), $user->getInterventionTracks());
    }

    /**
     * Test a modification of a user and assert that data has been changed
     */
    public function testPutUserApi()
    {
        
        $this->testLogin(); // pour avoir le jwt

        $jwtPayload = json_decode(base64_decode(explode(".", $this->jwtToken)[1]));

        self::ensureKernelShutdown();
        $client = static::createClient();
        $serializer = new Serializer(array(new GetSetMethodNormalizer()), array(new JsonEncoder('json')));
        
        $client->request('GET', '/api/users/4', array(), array(), array('CONTENT_TYPE' => 'application/json', 'HTTP_Authorization' => 'Bearer ' . $this->jwtToken, 'HTTP_x-xsrf-token' => $jwtPayload->csrfToken));
        $response = $client->getResponse()->getContent();
        $userResponse = $serializer->deserialize($response, User::class, 'json');

        $client->request(
            'PUT',
            '/api/users/' . $userResponse->getId(),
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json', 'HTTP_Authorization' => 'Bearer ' . $this->jwtToken, 'HTTP_x-xsrf-token' => $jwtPayload->csrfToken),
            json_encode(array(
                "firstname" => "Put",
                "lastname" => "Test"
            ))
        );
        $response = $client->getResponse()->getContent();
        $modifUserResponse = $serializer->deserialize($response, User::class, 'json');
        
        $this->assertNotEquals($modifUserResponse->getFirstname(), $userResponse->getFirstname());
        $this->assertEquals($modifUserResponse->getEmail(), $userResponse->getEmail());
        $this->assertNotEquals($modifUserResponse->getLastname(), $userResponse->getLastname());
        $this->assertEquals($modifUserResponse->getPassword(), $userResponse->getPassword());
        $this->assertEquals($modifUserResponse->getPhone(), $userResponse->getPhone());
        $this->assertEquals($modifUserResponse->getRoles(), $userResponse->getRoles());
        $this->assertNotEquals($modifUserResponse->getUsername(), $userResponse->getUsername());
        $this->assertEquals($modifUserResponse->getInterventions(), $userResponse->getInterventions());
        $this->assertEquals($modifUserResponse->getAddress(), $userResponse->getAddress());
        $this->assertEquals($modifUserResponse->getInterventionTracks(), $userResponse->getInterventionTracks());
    }
}
