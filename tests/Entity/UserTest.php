<?php

// tests/Entity/UserTest.php
namespace tests\Entity;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserTest extends KernelTestCase
{
    private $encoder;

    protected function setUp() : void
    {
        self::bootKernel();
        $this->encoder = self::$container->get('Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface');
    }

    public function testUserCreate()
    {
        $user = new User();
        $user->setAddress("24 allé du bois");
        $user->setFirstname("Ano");
        $user->setLastname("NYMOUS");
        $user->setPhone("0600000000");
        $user->setEmail("ano.nymous@viacesi.fr");
        $pass = $this->encoder->encodePassword($user, "anon");
        $user->setPassword($pass);
        $user->setRoles(['ROLE_ADMIN']);

        $this->assertEquals("24 allé du bois", $user->getAddress());
        $this->assertEquals("Ano", $user->getFirstname());
        $this->assertEquals("NYMOUS", $user->getLastname());
        $this->assertEquals("0600000000", $user->getPhone());
        $this->assertEquals($pass, $user->getPassword());
        $this->assertContains('ROLE_ADMIN', $user->getRoles());
    }
}
