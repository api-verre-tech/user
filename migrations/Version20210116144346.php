<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210116144346 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE intervention (id INT AUTO_INCREMENT NOT NULL, fkid_u_id INT NOT NULL, fkid_inter_type_id INT NOT NULL, message VARCHAR(255) NOT NULL, time DATETIME NOT NULL, handled TINYINT(1) NOT NULL, INDEX IDX_D11814ABB6180081 (fkid_u_id), INDEX IDX_D11814AB2E67FC0F (fkid_inter_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE intervention_track (id INT AUTO_INCREMENT NOT NULL, fkid_u_id INT NOT NULL, fkid_inter_id INT NOT NULL, message VARCHAR(255) NOT NULL, time DATETIME NOT NULL, INDEX IDX_A386E422B6180081 (fkid_u_id), INDEX IDX_A386E4222BA646E0 (fkid_inter_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE intervention_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, phone VARCHAR(15) NOT NULL, roles JSON NOT NULL, UNIQUE INDEX users_email_unique (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE intervention ADD CONSTRAINT FK_D11814ABB6180081 FOREIGN KEY (fkid_u_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE intervention ADD CONSTRAINT FK_D11814AB2E67FC0F FOREIGN KEY (fkid_inter_type_id) REFERENCES intervention_type (id)');
        $this->addSql('ALTER TABLE intervention_track ADD CONSTRAINT FK_A386E422B6180081 FOREIGN KEY (fkid_u_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE intervention_track ADD CONSTRAINT FK_A386E4222BA646E0 FOREIGN KEY (fkid_inter_id) REFERENCES intervention (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE intervention_track DROP FOREIGN KEY FK_A386E4222BA646E0');
        $this->addSql('ALTER TABLE intervention DROP FOREIGN KEY FK_D11814AB2E67FC0F');
        $this->addSql('ALTER TABLE intervention DROP FOREIGN KEY FK_D11814ABB6180081');
        $this->addSql('ALTER TABLE intervention_track DROP FOREIGN KEY FK_A386E422B6180081');
        $this->addSql('DROP TABLE intervention');
        $this->addSql('DROP TABLE intervention_track');
        $this->addSql('DROP TABLE intervention_type');
        $this->addSql('DROP TABLE users');
    }
}
